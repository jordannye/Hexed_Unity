﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClearStageData : MonoBehaviour
{
    public GameLoader gameLoader;

    public Text buttonText;

    public string firstStageText;
    public string secondStageText;
    public string clearedText;

    private int clickCount = 0;

    public float buttonResetTime;
    private float resetTime;

	// Use this for initialization
	void Start ()
    {
        buttonText.text = firstStageText;
        clickCount = 0;
        resetTime = buttonResetTime;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (clickCount > 0)
        {
            resetTime -= Time.deltaTime;
        }	

        if( resetTime <= 0)
        {
            resetTime = buttonResetTime;
            clickCount = 0;
            buttonText.text = firstStageText;
        }

	}

    public void clearDataClick()
    {
        clickCount++;
        switch(clickCount)
        {
            case 1:
                buttonText.text = secondStageText;
                resetTime = buttonResetTime;
                break;
            case 2:
                buttonText.text = clearedText;
                resetTime = buttonResetTime*3;
                clearAllStageData();
                break;
        }
    }

    private void clearAllStageData()
    {
        PlayerData.playerData.stageLevel = 0;
        PlayerPrefs.SetInt("StageLevel", PlayerData.playerData.stageLevel);
        PlayerPrefs.Save();

        gameLoader.leaveStageMode();
    }
}
