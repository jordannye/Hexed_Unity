﻿using UnityEngine;
using System.Collections;

public class MoveHexagonHighlight : MonoBehaviour
{
    //These coords is the x,y location of the currently highlighted node
    [SerializeField]
    private int[] highlightHexNode = new int[] { 0, 0 };

    [SerializeField]
    private GameObject mainHighLight;

    [SerializeField]
    private GameObject childHighlights;

    [SerializeField]
    private TextMesh comboText;

    private GameObject[] winHighlights = new GameObject[] {null};
    
    /// <summary>
    /// A node has been clicked, you need to move the highlight there
    /// </summary>
    /// <param name="hexNode"></param>
    public void hexNodeClicked(HexNodeScript hexNode)
    {
        int x = hexNode.gridX;
        int y = hexNode.gridY;

        Vector2 pos = CreateGrid.grid.convertGridPositionToWorldPosition(x,y);
        mainHighLight.transform.position = new Vector3(pos.x, pos.y, mainHighLight.transform.position.z);


        comboText.transform.position = new Vector3(pos.x, pos.y, comboText.transform.position.z);

        highlightHexNode[0] = x;
        highlightHexNode[1] = y;

        destroyHighlightChildren();

        createChildNodesForPossibleWin(hexNode);
    }

    public void hexNodeWinClick(HexNodeScript hexNode)
    {
        destroyHighlightChildren();
        mainHighLight.transform.position = new Vector3(-5, -5, -10);
        highlightHexNode = new int[] { -1, -1 };

        comboText.text = "";
    }

    private void createChildNodesForPossibleWin(HexNodeScript hexNode)
    {
        int x = hexNode.gridX;
        int y = hexNode.gridY;

        comboText.text = "";

        //Grab all of the connecting nodes to this node
        ArrayList nodes = GetAllNodesOfSameType.getNodes.getAllConnectingNodes(hexNode.tag, x, y);
        if (nodes == null || nodes.Count == 0) return;

        comboText.text = nodes.Count.ToString();

        winHighlights = new GameObject[nodes.Count];

        HexNodeScript childNode = null;
        int gridX, gridY;
        //For each child node, make a new instance of the highlight and put it on top
        for (int i = 0; i < nodes.Count; i++)
        {
            childNode = ((GameObject)nodes[i]).GetComponent<HexNodeScript>();
            gridX = childNode.gridX;
            gridY = childNode.gridY;

            winHighlights[i] = Instantiate(childHighlights);
            Vector2 childPos = CreateGrid.grid.convertGridPositionToWorldPosition(gridX, gridY);

            winHighlights[i].transform.position = new Vector3(childPos.x, childPos.y, mainHighLight.transform.position.z);
        }
    }
    
    private void destroyHighlightChildren()
    {
        //Destroy all of the previous connecting nodes
        for (int i = 0; i < winHighlights.Length; i++)
        {
            if (winHighlights[i] != null) Destroy(winHighlights[i].gameObject);
        }
    }

    public int[] getHighlightGridPostiion()
    {
        return highlightHexNode;
    }

    public GameObject getHexHighlight()
    {
        return mainHighLight;
    }
}
