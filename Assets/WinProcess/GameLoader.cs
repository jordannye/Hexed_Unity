﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{
    public GameObject timeOutPanel;
    public Text sadFace;
    public ShowChallengeInformation showChallengeInfo;

    public ComputerPlayer compPlayer;

    void Update()
    {
        if( Input.GetKeyDown("escape"))
        {
            if (PlayerData.playerData.isStageMode()) leaveStageMode();
            else if (PlayerData.playerData.isEndlessMode()) leaveEndlessMode();
            else if (PlayerData.playerData.isCPUMode()) leaveCPUMode();
            else if (PlayerData.playerData.isHowToMode()) leaveHowToMode();

            else if (PlayerData.playerData.isTitleScreen()) exitGame();
        }
    }

    //---------------------------------------------------------------------------------------
    // STAGE MODE
    public void loadStageMode()
    {
        PlayerData.playerData.currentScore = 0;
        PlayerData.playerData.setGameType(PlayerData.GAME_TYPE.STAGE);
        PlayerData.playerData.loadNextChallenge(PlayerData.playerData.getStageLevelCalculationComponent());
        savePlayerData();
        CreateGrid.grid.createGrid(PlayerData.playerData.getStageLevelCalculationComponent());

        SceneManager.LoadScene("Stage_mode");
    }
    
    public void leaveStageMode(bool completedChallenge = false)
    {
        if (completedChallenge)
        {
            PlayerData.playerData.clearChallengeData();
            savePlayerData();
            PlayerData.playerData.loadNextChallenge(PlayerData.playerData.getStageLevelCalculationComponent());
            if (showChallengeInfo != null) showChallengeInfo.showChallengeInformation();
        }
        else
        {
            CreateGrid.grid.createGrid(PlayerData.playerData.stageLevel);
            loadTitleScreen();
        }
    }

    public void timeOutStageMode()
    {
        if(timeOutPanel!= null)
        {
            timeOutPanel.SetActive(true);
        }
    }

    public void timeOutCPUMode()
   {
        bool win = PlayerData.playerData.currentScore > PlayerData.playerData.CPUScore;
        if (win) sadFace.text = "Congratulations you just beat the computer :)";
        else sadFace.text = "You just got beat by a computer, sucks to be you";

        Destroy(compPlayer);

        timeOutPanel.SetActive(true);
    }


    //---------------------------------------------------------------------------------------
    // ENDLESS MODE

    public void loadEndlessMode()
    {
        PlayerData.playerData.setGameType(PlayerData.GAME_TYPE.ENDLESS);

        CreateGrid.grid.createGrid(0);
        SceneManager.LoadScene("Endless_mode");

        PlayerData.playerData.endlessScore = 0;
        this.loadNextChallenge();
    }

    public void loadNextChallenge()
    {
        PlayerData.playerData.loadNextChallenge((int)(Time.time * 555.0f));
        if (showChallengeInfo != null) showChallengeInfo.showChallengeInformation();
    }

    public void leaveEndlessMode()
    {
        PlayerData pd = PlayerData.playerData;

        if( pd.endlessScore > pd.endlessHighScore ) pd.endlessHighScore = pd.endlessScore;

        PlayerData.playerData.endlessScore = 0;

        CreateGrid.grid.createGrid(0);
        loadTitleScreen();
    }


    //----------------------------------------------------------------------------------------
    //VS cpu mode

    public void loadVSCPUMode()
    {
        PlayerData.playerData.setGameType(PlayerData.GAME_TYPE.CPU);
        SceneManager.LoadScene("vsCPU_mode");
    }

    public void leaveCPUMode()
    {
        loadTitleScreen();
    }


    //------------------------------------------------------------------------------------------
    // How To Mode

    public void loadHowToMode()
    {
        PlayerData.playerData.setGameType(PlayerData.GAME_TYPE.HowTo);
        SceneManager.LoadScene("HowTo_mode");
    }

    public void leaveHowToMode()
    {
        loadTitleScreen();
    }



    private void loadTitleScreen()
    {
        PlayerData.playerData.setGameType(PlayerData.GAME_TYPE.TITLE);
        PlayerData.playerData.challengeCount = 0;
        savePlayerData();
        SceneManager.LoadScene("TitleScreen");
    }

    private void savePlayerData()
    {
        PlayerData.playerData.storePlayerPrefs();   
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
