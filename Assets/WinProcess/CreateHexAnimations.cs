﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CreateHexAnimations : MonoBehaviour
{
    public GameObject winContainer;
    
    public ChallengeController challengeController;
    public GameScore gameScore;
    public AddWinToGameScore addGameScore;

    public GameObject blueHexAnimationDestroyAnimation;
    public GameObject greenHexAnimationDestroyAnimation;
    public GameObject orangeHexAnimationDestroyAnimation;
    public GameObject purpleHexAnimationDestroyAnimation;
    public GameObject redHexAnimationDestroyAnimation;
    public GameObject yellowHexAnimationDestroyAnimation;

    //private ArrayList thisWin;

    //public delegate void PlayThisAnimation(SpriteSequenceAnimator animator);

    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    //void HexNodeClicked(Collider2D hexCollider)
    //{

    /// <summary>
    /// Starts the process to remove the hexagons from the grid and play the destroy animation on top
    /// Requires the winlist created from the getAllNodes.getAllConnectingNodes(tag,x,y);
    /// </summary>
    /// <param name="win"></param>
    public SpriteSequenceAnimator[] generateWinAnimators(WinAnimationDataObject dataObj)
    { 
        //Add the total win you are about to show as a score.
        //addGameScore.addWinScoreTotal(win);

        //Setup the win animators.
        SpriteSequenceAnimator[] animators = new SpriteSequenceAnimator[dataObj.currentWinCoords.Length];

        //thisWin = win;

        //Creates the first destroy animation, now you need to add each one as the same Making sure to add the next one to start when the one before it finishes
        for (int i = 0; i < dataObj.currentWinCoords.Length; i++)
        {
            //HexNodeScript hexNode = ((GameObject)win[i]).GetComponent<HexNodeScript>();
            HexNodeScript hexNode = CreateGrid.grid.GAME_GRID[dataObj.currentWinCoords[i][0], dataObj.currentWinCoords[i][1]].GetComponent<HexNodeScript>();
            hexNode.active = false;

            SpriteSequenceAnimator animator = createDestroyAnimator(hexNode);
            
            animators[i] = animator;
        }

        for (int i = 0; i < animators.Length - 1; i++)
        {
            animators[i].addProgressCallback(new PlaySequenceAnimationCallback(animators[i+1]));
        }
        return animators;
        
    }

    public SpriteSequenceAnimator[] generateSpawnAnimations(WinAnimationDataObject dataObj)
    {
        //Add the total win you are about to show as a score.
        //addGameScore.addWinScoreTotal(win);

        //Setup the win animators.
        SpriteSequenceAnimator[] animators = new SpriteSequenceAnimator[dataObj.currentWinCoords.Length];

        //thisWin = win;

        //Creates the first destroy animation, now you need to add each one as the same Making sure to add the next one to start when the one before it finishes
        for (int i = 0; i < dataObj.currentWinCoords.Length; i++)
        {
            SpriteSequenceAnimator animator = createSpawnAnimator(i, dataObj);
            animators[i] = animator;
        }

        return animators;
    }

    public SpriteSequenceAnimator createSpawnAnimator(int index, WinAnimationDataObject dataObj)
    {
        int gridX = dataObj.currentWinCoords[index][0];
        int gridY = dataObj.currentWinCoords[index][1];

        GameObject spawnAnimation = getDestroyAnimation(CreateGrid.grid.GAME_GRID[gridX,gridY].tag);
        SpriteSequenceAnimator seqAnimator = spawnAnimation.GetComponent<SpriteSequenceAnimator>();

        Vector2 newPos = CreateGrid.grid.convertGridPositionToWorldPosition(gridX, gridY);// new Vector2(grid.GAME_GRID[gridX, gridY].transform.position.x, grid.GAME_GRID[gridX, gridY].transform.position.y);
        seqAnimator.transform.position = newPos;

        seqAnimator.setGridPosition(gridX, gridY);

        seqAnimator.setReversed(true);

        seqAnimator.transform.parent = winContainer.transform;

        return seqAnimator;
    }

    private SpriteSequenceAnimator createDestroyAnimator(HexNodeScript hexNode)
    {
        int gridX = hexNode.gridX;
        int gridY = hexNode.gridY;

        GameObject destroyAnimation = getDestroyAnimation(hexNode.tag);
        SpriteSequenceAnimator seqAnimator = destroyAnimation.GetComponent<SpriteSequenceAnimator>();

        Vector2 newPos = new Vector2(CreateGrid.grid.GAME_GRID[gridX, gridY].transform.position.x, CreateGrid.grid.GAME_GRID[gridX, gridY].transform.position.y);
        seqAnimator.transform.position = newPos;
        
        seqAnimator.setGridPosition(gridX, gridY);

        seqAnimator.transform.parent = winContainer.transform;

        return seqAnimator;
    }

    private GameObject getDestroyAnimation(string tag)
    {
        if( tag.Equals("Blue"))
        {
            return Instantiate(blueHexAnimationDestroyAnimation);
        }
        else if (tag.Equals("Green"))
        {
            return Instantiate(greenHexAnimationDestroyAnimation);
        }
        else if (tag.Equals("Orange"))
        {
            return Instantiate(orangeHexAnimationDestroyAnimation);
        }
        else if (tag.Equals("Purple"))
        {
            return Instantiate(purpleHexAnimationDestroyAnimation);
        }
        else if (tag.Equals("Red"))
        {
            return Instantiate(redHexAnimationDestroyAnimation);
        }
        else// if (tag.Equals("Yellow"))
        {
            return Instantiate(yellowHexAnimationDestroyAnimation);
        }
    }
}
