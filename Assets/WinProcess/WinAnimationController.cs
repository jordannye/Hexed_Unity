﻿using UnityEngine;
using System.Collections;

public class WinAnimationController : MonoBehaviour
{
    public GameController gameController;
    public PaintBrushController bombController;
    public ChallengeController challengeController;
    public GameScore gameScore;

    public CreateHexAnimations createHexAnimations;
    public PlayWinAnimation playWinAnimation;
    public PlayRespawnAnimation playRespawnAnimation;

    public GameTime time;

    private double winPerHex = 0;

    //private ArrayList totalWin;

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void StartWinAnimation(AddWinToGameScore addGameScore, WinAnimationDataObject dataObj)
    {
        if (dataObj.currentWinCoords == null || dataObj.currentWinCoords.Length <= 0) return;
        //totalWin = win;

        //This will calculate how much each win is worth per animation, now add this amount as a callback to add to the game score for each node that completes its win animation.
        winPerHex = (double)(dataObj.currentWinAmount / dataObj.currentWinCoords.Length);

        //Lock win animation
        gameController.LockWin();

        //Create the win animations
        SpriteSequenceAnimator[] animators = createHexAnimations.generateWinAnimators(dataObj);

        for (int i = 0; i < animators.Length; i++)
        {
            //Destroys the grid node object at current location.
            animators[i].addStartCallback(new DestroyGameObjectCallback(CreateGrid.grid.GAME_GRID[dataObj.currentWinCoords[i][0], dataObj.currentWinCoords[i][1]]));

            //Generate a new grid node object at the location and set its scale to 0
            animators[i].addStartCallback(new CreateWeightedGridNodeCallback(dataObj.currentWinCoords[i][0], dataObj.currentWinCoords[i][1], CreateGrid.grid, false));

            //To do during the animation--            
            //Add to the game score
            animators[i].addProgressCallback(new AddScoreToGameScoreCallback(addGameScore, winPerHex, dataObj.cpu));
            
            //Things to not do during CPU mode
            if (!PlayerData.playerData.isCPUMode())
            {
                //Extend game time
                animators[i].addProgressCallback(new AddTimeBasedOnSingleScoreCallback(winPerHex, time));
                //Increase recharge time for bombs.
                animators[i].addProgressCallback(new AddRechargeTimeForBombs(winPerHex, bombController));
            }

            //When each destroy animation finishes it will spawn the next one and set its scale back to default
            animators[i].addFinishCallback(new CreateSpawnAnimationCallback(i, createHexAnimations, dataObj,
                new ActivateGridSquareCallback(CreateGrid.grid, dataObj.currentWinCoords[i][0], dataObj.currentWinCoords[i][1], true) ));
        }

        //Play the win animation
        //Tell the game controller that you have finished a win animation.
        animators[animators.Length - 1].addFinishAction(WinAnimationsFinished);

        //Once the destroy animations have finished, check to see if you have completed a challenge :)
        int winSize = animators.Length;
        string winType = dataObj.currentWinTag;
        animators[animators.Length - 1].addFinishCallback(new CheckForCompleteChallengesCallback(challengeController, gameScore, winSize, winType, CreateGrid.grid));

        animators[0].Begin();
    }

    public void WinAnimationsFinished()
    {
        //Unlock win animation
        gameController.UnlockWin();

        gameController.WinAnimationFinished();
    }
}