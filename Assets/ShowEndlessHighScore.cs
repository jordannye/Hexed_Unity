﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowEndlessHighScore : MonoBehaviour
{
    public Text endlessHighScore;

	// Use this for initialization
	void Start ()
    {
        endlessHighScore.text = PlayerData.playerData.endlessHighScore.ToString();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
