﻿using UnityEngine;
using System.Collections;

public class DestroyObjectColliding : MonoBehaviour
{
    void OnTriggerExit2D(Collider2D other)
    {
        Destroy(other.gameObject);
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        Destroy(coll.gameObject);
    }
}
