﻿using UnityEngine;
using System.Collections;

public class CreateNewFlyingHexagon : MonoBehaviour
{
    public GameObject hexTemplate;

    public Sprite[] hexagons;

    [SerializeField]
    private double timeBetweenHexSpawn = 10.0;//Decreases by 1, ten times
    [SerializeField]
    private double finalTimeBetweenHexSpawns = 0.5;

    [SerializeField]
    private double currentTime = 0.0;

    [SerializeField]
    private double waitTilStart = 5.0;//Seconds
    private bool started = false;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!started)
        {
            waitTilStart -= Time.deltaTime;

            if (waitTilStart <= 0.0)
            {
                started = true;
                spawnRandomHexagon();
            }
        }
        else
        {
            currentTime += Time.deltaTime;

            if (currentTime >= timeBetweenHexSpawn)
            {
                if (timeBetweenHexSpawn > finalTimeBetweenHexSpawns) timeBetweenHexSpawn -= finalTimeBetweenHexSpawns;
                else timeBetweenHexSpawn = finalTimeBetweenHexSpawns;

                currentTime = 0.0;
                spawnRandomHexagon();
            }
        }
    }

    private void spawnRandomHexagon()
    {
        int x = Random.Range(-3, 3);
        int y = Random.Range(5, 9);
        GameObject gameObj = Instantiate(hexTemplate);

        gameObj.transform.position = this.gameObject.transform.position;

        gameObj.transform.position = new Vector3(x, y);
        SpriteRenderer spriteRender = gameObj.GetComponent<SpriteRenderer>();
        Sprite sprite = hexagons[Random.Range(0, hexagons.Length)];

        spriteRender.sprite = sprite;

    }
}
