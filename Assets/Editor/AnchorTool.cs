﻿using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    public class AnchorToolContext
    {
        [MenuItem("CONTEXT/RectTransform/Anchors To Bounds")]
        public static void SetAnchorsToRectBounds(MenuCommand command)
        {
            _OwnRectTransform = null;
            _ParentRectTransform = null;

            _OwnRectTransform = command.context as RectTransform;
            if (_OwnRectTransform == null) return;

            _ParentRectTransform = _OwnRectTransform.parent?.GetComponent<RectTransform>();
            if (_ParentRectTransform == null) return;

            CalculateCurrentWH();
            CalculateCurrentXY();
            AnchorsToCorners();
        }

        public static Rect AnchorRect;
        public static Vector2 AnchorVector;

        private static Rect _AnchorRectOld;
        private static Vector2 _AnchorVectorOld;
        private static Vector2 _PivotOld;
        private static Vector2 _OffsetMinOld;
        private static Vector2 _OffsetMaxOld;

        private static RectTransform _OwnRectTransform;
        private static RectTransform _ParentRectTransform;

        private static void CalculateCurrentXY()
        {
            float pivotX = AnchorRect.width * _OwnRectTransform.pivot.x;
            float pivotY = AnchorRect.height * (1 - _OwnRectTransform.pivot.y);
            Vector2 newXY = new Vector2(_OwnRectTransform.anchorMin.x * _ParentRectTransform.rect.width + _OwnRectTransform.offsetMin.x + pivotX - _ParentRectTransform.rect.width * AnchorVector.x,
                -(1 - _OwnRectTransform.anchorMax.y) * _ParentRectTransform.rect.height + _OwnRectTransform.offsetMax.y - pivotY + _ParentRectTransform.rect.height * (1 - AnchorVector.y));
            AnchorRect.x = newXY.x;
            AnchorRect.y = newXY.y;
            _AnchorRectOld = AnchorRect;
        }

        private static void CalculateCurrentWH()
        {
            AnchorRect.width = _OwnRectTransform.rect.width;
            AnchorRect.height = _OwnRectTransform.rect.height;
            _AnchorRectOld = AnchorRect;
        }

        private static void AnchorsToCorners()
        {
            float pivotX = AnchorRect.width * _OwnRectTransform.pivot.x;
            float pivotY = AnchorRect.height * (1 - _OwnRectTransform.pivot.y);
            _OwnRectTransform.anchorMin = new Vector2(0f, 1f);
            _OwnRectTransform.anchorMax = new Vector2(0f, 1f);
            _OwnRectTransform.offsetMin = new Vector2(AnchorRect.x / _OwnRectTransform.localScale.x, AnchorRect.y / _OwnRectTransform.localScale.y - AnchorRect.height);
            _OwnRectTransform.offsetMax = new Vector2(AnchorRect.x / _OwnRectTransform.localScale.x + AnchorRect.width, AnchorRect.y / _OwnRectTransform.localScale.y);
            _OwnRectTransform.anchorMin = new Vector2(_OwnRectTransform.anchorMin.x + AnchorVector.x + (_OwnRectTransform.offsetMin.x - pivotX) / _ParentRectTransform.rect.width * _OwnRectTransform.localScale.x,
                _OwnRectTransform.anchorMin.y - (1 - AnchorVector.y) + (_OwnRectTransform.offsetMin.y + pivotY) / _ParentRectTransform.rect.height * _OwnRectTransform.localScale.y);
            _OwnRectTransform.anchorMax = new Vector2(_OwnRectTransform.anchorMax.x + AnchorVector.x + (_OwnRectTransform.offsetMax.x - pivotX) / _ParentRectTransform.rect.width * _OwnRectTransform.localScale.x,
                _OwnRectTransform.anchorMax.y - (1 - AnchorVector.y) + (_OwnRectTransform.offsetMax.y + pivotY) / _ParentRectTransform.rect.height * _OwnRectTransform.localScale.y);
            _OwnRectTransform.offsetMin = new Vector2((0 - _OwnRectTransform.pivot.x) * AnchorRect.width * (1 - _OwnRectTransform.localScale.x), (0 - _OwnRectTransform.pivot.y) * AnchorRect.height * (1 - _OwnRectTransform.localScale.y));
            _OwnRectTransform.offsetMax = new Vector2((1 - _OwnRectTransform.pivot.x) * AnchorRect.width * (1 - _OwnRectTransform.localScale.x), (1 - _OwnRectTransform.pivot.y) * AnchorRect.height * (1 - _OwnRectTransform.localScale.y));

            _OffsetMinOld = _OwnRectTransform.offsetMin;
            _OffsetMaxOld = _OwnRectTransform.offsetMax;
        }
    }
}