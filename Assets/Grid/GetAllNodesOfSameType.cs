﻿ using UnityEngine;
using System.Collections;

public class GetAllNodesOfSameType : MonoBehaviour
{
    public static GetAllNodesOfSameType getNodes;

    public Vector3 _mousePosition;

    public Vector2 _mouseGridPosition;

    void Awake()
    {
        //Creates a singleton of this class, you can only have 1 active player data class at once.
        if (getNodes == null)
        {
            DontDestroyOnLoad(gameObject);
            getNodes = this;
        }
        else if (getNodes != this)
        {
            Destroy(this.gameObject);
        }
    }

    void HexMouseHover(Collider2D hexCollider)
    {
        HexNodeScript hxScript = hexCollider.gameObject.GetComponent<HexNodeScript>();
        int startX = hxScript.gridX;
        int startY = hxScript.gridY;
        _mouseGridPosition = new Vector2(startX, startY);
    }

    private RaycastHit2D objectAtMousePosition()
    {
        return Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
    }

    public ArrayList getAllConnectingNodes(string hexTag, int x, int y)
    {
        //Get all the originally connecting nodes, including the first one.
        ArrayList neighbourNodes = getNeighboursOfTypeIncludeSelf(hexTag, x, y);

        if (neighbourNodes == null || neighbourNodes.Count == 0 ) return null;

        //currentWin will now only hold nodes that lead to a dead end and the start
        //A dead end is a node with no more neighbours of the same type
        ArrayList currentWin = new ArrayList();
        currentWin.Add(neighbourNodes[0]);

        neighbourNodes.RemoveAt(0);

        //While you still have neighbours to check, keep adding them to the win
        while (neighbourNodes.Count > 0)
        {
            //Grab the first neighbour to check for more neighbours, remove it from the neighbour list and add it to the win list.
            HexNodeScript currentNode = ((GameObject)neighbourNodes[0]).GetComponent<HexNodeScript>();
            currentWin.Add(neighbourNodes[0]);
            neighbourNodes.RemoveAt(0);

            //Ask for all its neighbours, if the array it returns back has more than 1 thing in it then remove the first one (as that is itself) and then add them to the neighbournodes
            ArrayList temp = getNeighboursOfTypeIncludeSelf(hexTag, currentNode.gridX, currentNode.gridY);
            if (temp != null )
            {
                //Before adding the neighbours into the neighbour array, make sure they are not already part of the currentWin or neighbourNodes
                for (int i = 0; i < temp.Count; i++)
                {
                    HexNodeScript testNode = ((GameObject)temp[i]).GetComponent<HexNodeScript>();

                    //If the neighbouring node isn't already part of the currentWin or the neighbouring nodes, then add it to the neighbouringNodes
                    if (!isThisNodeInHere(testNode.gridX,testNode.gridY, currentWin) &&
                        !isThisNodeInHere(testNode.gridX, testNode.gridY, neighbourNodes) )
                    {
                        neighbourNodes.Add(temp[i]);
                    }
                }
            }
        }

        //After following all the neighbours, does your win have more nodes than the minimum required for that type?
        if (currentWin.Count < WinCalculations.winCals.getMinSymbolsInWin(hexTag)) return null;
        return (ArrayList)currentWin.Clone();
    }

    /// <summary>
    /// Checks to see if the node at the X and Y coordinates given is within the array of nodes that you give in.
    /// True it is inside the array false otherwise.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="listOfNodes"></param>
    /// <returns></returns>
    private bool isThisNodeInHere(int x, int y, ArrayList listOfNodes)
    {
        for( int i =0; i < listOfNodes.Count; i++)
        {
            HexNodeScript node = ((GameObject)listOfNodes[i]).GetComponent<HexNodeScript>();
            if (node.gridX == x && node.gridY == y) return true;
        }
        return false;
    }

    /**
    * Will retrieve you all of the neighbouring nodes of the same type as the information given in, including the root node given.
    **/
    public ArrayList getNeighboursOfTypeIncludeSelf(string hexTag, int x, int y)
    {
        ArrayList ary = new ArrayList();
        addToArrayIfNotNullAndSameTag(ary, hexTag, x, y);

        addToArrayIfNotNullAndSameTag(ary, hexTag, x + 1, y);
        addToArrayIfNotNullAndSameTag(ary, hexTag, x - 1, y);

        if (y % 2 == 0)
        {
            addToArrayIfNotNullAndSameTag(ary, hexTag, x - 1, y - 1);
            addToArrayIfNotNullAndSameTag(ary, hexTag, x, y - 1);

            addToArrayIfNotNullAndSameTag(ary, hexTag, x - 1, y + 1);
            addToArrayIfNotNullAndSameTag(ary, hexTag, x, y + 1);
        }
        else
        {
            addToArrayIfNotNullAndSameTag(ary, hexTag, x + 1, y - 1);
            addToArrayIfNotNullAndSameTag(ary, hexTag, x, y - 1);

            addToArrayIfNotNullAndSameTag(ary, hexTag, x + 1, y + 1);
            addToArrayIfNotNullAndSameTag(ary, hexTag, x, y + 1);
        }

        return ary;
    }
    private void addToArrayIfNotNullAndSameTag(ArrayList thisArray, string rootTag, int x, int y)
    {
        GameObject obj = null;
        //The x and y you are checking has to be within the bounds of the grid.
        if (x >= 0 && x < (int)CreateGrid.GRID_WIDTH &&
            y >= 0 && y < (int)CreateGrid.GRID_HEIGHT)
        {
            obj = CreateGrid.grid.GAME_GRID[x, y];
            if (obj != null)
            {
                string thisTag = obj.tag;
                HexNodeScript hexNode = obj.GetComponent<HexNodeScript>();
                if (!thisTag.Equals(rootTag) || !hexNode.active ) return;

                thisArray.Add(obj);
            }
        }
    }


    /// <summary>
    /// Gets all the neighbours of this node if they aren't null, not including self
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public ArrayList getNeighboursOfGridNode(int x, int y)
    {
        ArrayList ary = new ArrayList();

        addToArrayIfNotNull(ary, x + 1, y);
        addToArrayIfNotNull(ary, x - 1, y);

        if (y % 2 == 0)
        {
            addToArrayIfNotNull(ary, x - 1, y - 1);
            addToArrayIfNotNull(ary, x, y - 1);

            addToArrayIfNotNull(ary, x - 1, y + 1);
            addToArrayIfNotNull(ary, x, y + 1);
        }
        else
        {
            addToArrayIfNotNull(ary, x + 1, y - 1);
            addToArrayIfNotNull(ary, x, y - 1);

            addToArrayIfNotNull(ary, x + 1, y + 1);
            addToArrayIfNotNull(ary, x, y + 1);
        }

        return ary;
    }

    private void addToArrayIfNotNull(ArrayList thisArray, int x, int y)
    {
        GameObject obj = null;
        //The x and y you are checking has to be within the bounds of the grid.
        if (x >= 0 && x < (int)CreateGrid.GRID_WIDTH &&
            y >= 0 && y < (int)CreateGrid.GRID_HEIGHT)
        {
            obj = CreateGrid.grid.GAME_GRID[x, y];
            if (obj != null)
            {
                string thisTag = obj.tag;
                HexNodeScript hexNode = obj.GetComponent<HexNodeScript>();

                if (hexNode == null) return;

                thisArray.Add(obj);
            }
        }
    }
}
