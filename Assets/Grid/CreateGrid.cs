﻿using UnityEngine;
using System.Collections;
using System;

public class CreateGrid : MonoBehaviour
{
    public static CreateGrid grid;

    public GameObject[] HexNodeTypes;

    public float NODE_WIDTH = 2;
    public float NODE_HEIGHT = 1.5f;
   
    public static int GRID_WIDTH = 14;
    public static int GRID_HEIGHT = 11;

    public GameObject[,] GAME_GRID = new GameObject[GRID_WIDTH, GRID_HEIGHT];

    void Awake()
    {
        //Creates a singleton of this class, you can only have 1 active player data class at once.
        if (grid == null)
        {
            grid = this;
        }
        else if (grid != this)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator ExecuteAfterTime(float time, Action functionCall)
    {
        yield return new WaitForSeconds(time);

        functionCall.Invoke();
    }

    // Use this for initialization
    void Start () 
    {
        createGrid((int)(Time.realtimeSinceStartup / Time.renderedFrameCount));
    }
    public void createGrid(int seed)
    {
        destroyGridNodes();
        UnityEngine.Random.seed = seed;

        for (int y = 0; y < GRID_HEIGHT; y++)
        {
            for (int x = 0; x < GRID_WIDTH; x++)
            {
                GAME_GRID[x, y] = generateRandomNode(x, y);
                GAME_GRID[x, y].GetComponent<HexNodeScript>().active = false;
            }
        }

        StartCoroutine(ExecuteAfterTime(1.5f, activateGrid));
    }

    private void activateGrid()
    {
        for (int y = 0; y < GRID_HEIGHT; y++)
        {
            for (int x = 0; x < GRID_WIDTH; x++)
            {
                if(GAME_GRID[x,y] != null)GAME_GRID[x, y].GetComponent<HexNodeScript>().active = true;
            }
        }
    }

    public void destroyGridNodes()
    {
        for (int y = 0; y < GRID_HEIGHT; y++)
        {
            for (int x = 0; x < GRID_WIDTH; x++)
            {
                if (GAME_GRID[x, y] != null) UnityEngine.Object.DestroyObject(GAME_GRID[x, y].gameObject);
                GAME_GRID[x, y] = null;
            }
        }
    }

    public Vector2 convertGridPositionToWorldPosition(int x, int y)
    {
        float worldX = x * NODE_WIDTH;
        if (y % 2 == 0 ) worldX -= NODE_WIDTH / 2;

        float worldY = -(y * NODE_HEIGHT);

        return new Vector2(worldX, worldY);
    }

    public GameObject generateRandomNode(int x, int y)
    {
        Vector2 pos = convertGridPositionToWorldPosition(x, y);

        GameObject node = (GameObject)Instantiate(HexNodeTypes[UnityEngine.Random.Range(0, HexNodeTypes.Length)], pos, Quaternion.identity);

        HexNodeScript hexNode = node.GetComponent<HexNodeScript>();

        hexNode.gridX = x;
        hexNode.gridY = y;
        node.transform.parent = transform;

        return node;
    }

    /// <summary>
    /// Will generate a new grid node using the surrounding nodes to try to get a node of the same colour.
    /// Instead of just randomly generating a new node.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public GameObject generateNewGridNode(int x, int y )
    {
        Vector2 pos = convertGridPositionToWorldPosition(x, y);

        //Yellow, Green, Orange, Red, Blue, Purple
        int[] colourWeightings = generateNeighbourWeightings(x, y);
        //The index 
        int[] hexTypeIndexes = { 0, 1, 2, 3, 4, 5 };

        int index = WeightedRandom.ReturnRandomFromWeightings(colourWeightings, hexTypeIndexes);
        GameObject node = (GameObject)Instantiate(HexNodeTypes[index], 
            pos, Quaternion.identity);

        node.GetComponent<HexNodeScript>().gridX = x;
        node.GetComponent<HexNodeScript>().gridY = y;
        node.transform.parent = transform;

        return node;
    }

    /// <summary>
    /// Generates a weighting int[] depending on how many colours are around the coords you give
    /// weighting order will be Yellow, Green, Orange, Red, Blue, Purple
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private int[] generateNeighbourWeightings(int x, int y)
    {
        //Yellow, Green, Orange, Red, Blue, Purple
        int[] weightings = { 1, 1, 1, 1, 1, 1};

        int neighbourWeighting = 2;

        ArrayList nodes = GetAllNodesOfSameType.getNodes.getNeighboursOfGridNode(x, y);
        if(nodes != null )
        { 
            for (int i = 0; i < nodes.Count; i++)
            {
                HexNodeScript hexNode = ((GameObject)nodes[i]).GetComponent<HexNodeScript>();
                if (!hexNode.active) continue;

                switch(hexNode.tag)
                {
                    case "Yellow":
                        weightings[0] += neighbourWeighting;
                        break;
                    case "Green":
                        weightings[1] += neighbourWeighting;
                        break;
                    case "Orange":
                        weightings[2] += neighbourWeighting;
                        break;
                    case "Red":
                        weightings[3] += neighbourWeighting;
                        break;
                    case "Blue":
                        weightings[4] += neighbourWeighting;
                        break;
                    case "Purple":
                        weightings[5] += neighbourWeighting;
                        break;
                    default:
                        break;
                }
            }

        }

        return weightings;
    }

    public GameObject genereateObject(int x, int y, string type)
    {
        Vector2 pos = convertGridPositionToWorldPosition(x, y);
        GameObject node = (GameObject)Instantiate(getNodeFromType(type), pos, Quaternion.identity);

        node.GetComponent<HexNodeScript>().gridX = x;
        node.GetComponent<HexNodeScript>().gridY = y;
        node.transform.parent = transform;

        return node;
    }

    private GameObject getNodeFromType(string hexType)
    {
        for( int i =0; i < HexNodeTypes.Length;i++)
        {
            if( HexNodeTypes[i].tag.Equals(hexType) || HexNodeTypes[i].tag == hexType )
            {
                return HexNodeTypes[i];
            }
        }
        return null;
    }

    public bool doesGridContainer(string colourType)
    {
        for (int height = 0; height < GRID_HEIGHT; height++)
        {
            for (int width = 0; width < GRID_WIDTH; width++)
            {
                if (GAME_GRID[width, height].tag.Equals(colourType)) return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Searches the grid to see if there are any wins for the type you hand in.
    /// </summary>
    /// <param name="colourType"></param>
    /// <returns> true if there are wins of this type still</returns>
    public bool doesGridContainWins(string colourType)
    {
        for (int height = 0; height < GRID_HEIGHT; height++)
        {
            for (int width = 0; width < GRID_WIDTH; width++)
            {
                GameObject obj = GAME_GRID[width, height];
                if(obj != null && obj.tag.Equals(colourType))
                {
                    ArrayList win = GetAllNodesOfSameType.getNodes.getAllConnectingNodes(colourType, width, height);
                    if (win != null && win.Count > 0) return true;
                }
            }
        }
        return false;
    }
}
