﻿using UnityEngine;
using System.Collections;

public class SpawnAnimationsFinishedCallback : ICallback
{
    private WinAnimationController winAnimationController;

    public SpawnAnimationsFinishedCallback(WinAnimationController winAnimationController)
    {
        this.winAnimationController = winAnimationController;
    }

    public void Run()
    {
        this.winAnimationController.WinAnimationsFinished();
    }
}
