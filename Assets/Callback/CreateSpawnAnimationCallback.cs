﻿using UnityEngine;
using System.Collections;

public class CreateSpawnAnimationCallback : ICallback
{
    private int index;
    private CreateHexAnimations createHexAnimations;
    private CreateGrid grid;
    private ICallback finishCallback;
    private WinAnimationDataObject _dataObj;

    public CreateSpawnAnimationCallback(int index, CreateHexAnimations createHexAnimations, WinAnimationDataObject dataObj, ICallback finishCallback = null)
    {
        this.index = index;
        this.createHexAnimations = createHexAnimations;
        this._dataObj = dataObj;
        this.finishCallback = finishCallback;
    }

    public void Run()
    {
        SpriteSequenceAnimator animator = createHexAnimations.createSpawnAnimator(index,_dataObj);
        if (this.finishCallback != null) animator.addFinishCallback(this.finishCallback);          
        animator.Begin();
    }
}
