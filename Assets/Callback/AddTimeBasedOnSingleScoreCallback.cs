﻿using UnityEngine;
using System.Collections;
using System;

public class AddTimeBasedOnSingleScoreCallback : ICallback
{
    private GameTime time;
    private double winPerHex;

    public AddTimeBasedOnSingleScoreCallback(double winPerHex, GameTime time)
    {
        this.winPerHex = winPerHex;
        this.time = time;
    }

    public void Run()
    {
        if( this.time != null )
        {
            this.time.increaseTimeLeftBy(PlayerData.playerData.calculateTimeExtendFromScore(winPerHex));
        }
    }
}
