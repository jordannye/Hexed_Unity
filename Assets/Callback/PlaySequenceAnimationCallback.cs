﻿using UnityEngine;
using System.Collections;
using System;

public class PlaySequenceAnimationCallback : ICallback
{
    private SpriteSequenceAnimator spriteSequenceAnimator;

    public PlaySequenceAnimationCallback(SpriteSequenceAnimator spriteSequenceAnimator )
    {
        this.spriteSequenceAnimator = spriteSequenceAnimator;
    }

    void ICallback.Run()
    {
        //Before beginning the animation you must have already setup all of the information needed using the set functions.
        //This will only start the animation working, set its sprite to be something other than invisible
        spriteSequenceAnimator.Begin();
    }
}
