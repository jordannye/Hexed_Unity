﻿using UnityEngine;
using System.Collections;

public class AddScoreToGameScoreCallback : ICallback
{
    private AddWinToGameScore addGameScore;
    private double increment;

    private bool _cpuPlayer;

    public AddScoreToGameScoreCallback(AddWinToGameScore addGameScore, double increment, bool cpuPlayer)
    {
        this.addGameScore = addGameScore;
        this.increment = increment;

        _cpuPlayer = cpuPlayer;
    }

    public void Run()
    {
        this.addGameScore.addScoreToGameScore(this.increment, _cpuPlayer);
    }
}
