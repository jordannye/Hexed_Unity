﻿using UnityEngine;
using System.Collections;
using System;

public class CheckForCompleteChallengesCallback : ICallback
{
    private ChallengeController challengeController;
    private GameScore gameScore;
    private int currentWinSize;
    private string currentWinType;

    public CheckForCompleteChallengesCallback(ChallengeController chalController, GameScore gmeScore, int winSize, string winType, CreateGrid theGrid)
    {
        challengeController = chalController;
        gameScore = gmeScore;
        currentWinSize = winSize;
        currentWinType = winType;
    }


    public void Run()
    {
        if (challengeController != null)
        {
            challengeController.checkForChallenges(gameScore.Score, currentWinSize, currentWinType);
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
