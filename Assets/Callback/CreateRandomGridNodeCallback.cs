﻿using UnityEngine;
using System.Collections;

public class CreateWeightedGridNodeCallback : ICallback
{
    private int gridX, gridY;
    private CreateGrid grid;
    private bool _active;

    public CreateWeightedGridNodeCallback(int x, int y, CreateGrid grid, bool active)
    {
        gridX = x;
        gridY = y;
        this.grid = grid;
        _active = active;
    }

    public void Run()
    {
        GameObject node = grid.generateNewGridNode(gridX, gridY);
        node.SetActive(_active);

        grid.GAME_GRID[gridX, gridY] = node;
    }
}
