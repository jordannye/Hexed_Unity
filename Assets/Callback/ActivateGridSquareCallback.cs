﻿using UnityEngine;
using System.Collections;

public class ActivateGridSquareCallback : ICallback
{
    private CreateGrid grid;
    private int x, y;
    private bool _active;

    public ActivateGridSquareCallback(CreateGrid grid, int x, int y, bool active)
    {
        this.grid = grid;
        this.x = x;
        this.y = y;
        _active = active;
    }

    public void Run()
    {
        if (grid.GAME_GRID[this.x, this.y] != null)
        {
            grid.GAME_GRID[this.x, this.y].SetActive(_active);
            HexNodeScript hexNode = grid.GAME_GRID[this.x, this.y].GetComponent<HexNodeScript>();
            if (hexNode != null) hexNode.active = _active;
        }
    }
}
