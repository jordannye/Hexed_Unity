﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class GameTime : MonoBehaviour
{
    public Text showTime;
    
    [SerializeField]
    private double gameTimeSeconds = 0.0;
    private TimeSpan tSpan;

    private bool startGame = false;

    public GameLoader gameLoader;


    //__-----------------------------------
    //VISUAL TIMER
    public RectTransform visualTimer;
    public float size = 0.0f;

    private float secondWorth = 0.025f;
    private float endingSize = 0.0f;//340.815f;//This is how much size has to reach before theres nothing left
    
    // Use this for initialization
    void Start ()
    {
        startTimer();
    }

    public void startTimer()
    {
        int downFrom = PlayerData.playerData.getStartingRoundTime();
        gameTimeSeconds = downFrom;
        
        updateVisualTimer();

        startGame = true;
    }

    //private void updateTimeText()
    //{
    //    if (showTime != null)
    //    {
    //        tSpan = TimeSpan.FromSeconds(gameTimeSeconds);
    //        string time = string.Format("{1:D2}:{2:D2}",
    //            tSpan.Hours,
    //            tSpan.Minutes,
    //            tSpan.Seconds,
    //            tSpan.Milliseconds);
    //        showTime.text = time;
    //    }
    //}

    public void increaseTimeLeftBy(float addTimeSeconds)
    {
        gameTimeSeconds += addTimeSeconds;
        updateVisualTimer();
    }

    private void updateVisualTimer()
    {
        size = (float)((float)gameTimeSeconds / secondWorth);
        visualTimer.offsetMax = new Vector2(0.0f, size);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (startGame)
        {
            float deltaTime = Time.smoothDeltaTime;
            gameTimeSeconds -= deltaTime;

            updateVisualTimer();

            //If the size of the timer reaches he ending size then time out.
            if (gameTimeSeconds <= 0.0f)
            {
                if (PlayerData.playerData.isStageMode()) gameLoader.timeOutStageMode();
                else if (PlayerData.playerData.isCPUMode()) gameLoader.timeOutCPUMode();

                Destroy(this.gameObject);
            }

        }
	}
}
