﻿using System;

internal class AddRechargeTimeForBombs : ICallback
{
    private double winPerHex;
    private PaintBrushController bombControl;

    public AddRechargeTimeForBombs(double winPerHex, PaintBrushController bombController)
    {
        this.winPerHex = winPerHex;
        this.bombControl = bombController;
    }

    public void Run()
    {
        if (this.bombControl != null)
        {
            bombControl.updateBombPoints(winPerHex);
        }
    }
}