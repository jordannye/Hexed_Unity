﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AddWinToGameScore : MonoBehaviour
{
    public GameScore score;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text cpuScoreText;

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void addScoreToGameScore(double winScore, bool cpuPlayer = false)
    {
        addScore(winScore,cpuPlayer);
    }

    private void addScore(double addScore, bool cpuPlayer)
    {
        if (!cpuPlayer) score.Score += addScore;
        else score.CPUScore += addScore;
        
        if (PlayerData.playerData.isStageMode())
        {
            PlayerData.playerData.currentScore = score.Score;
        }
        else if (PlayerData.playerData.isEndlessMode())
        {
            PlayerData.playerData.endlessScore = score.Score;
        }
        else if ( PlayerData.playerData.isCPUMode() )
        {
            PlayerData.playerData.CPUScore = score.CPUScore;
            PlayerData.playerData.currentScore = score.Score;
        }

        if (!cpuPlayer) scoreText.text = score.Score.ToString("#.");
        else cpuScoreText.text = score.CPUScore.ToString("#.");
    }
}
