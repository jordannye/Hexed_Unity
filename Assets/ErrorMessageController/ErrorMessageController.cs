﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorMessageController : MonoBehaviour
{
    public Image errorMessagePanel;
    public Text errorMessageText;

    public WinCalculations winCals;

    public Image infoPanel;
    
    [SerializeField]
    private float visibleTime = 2.5f;
    public float currentTime = 0.0f;

    [SerializeField]
    private float messageDropSpeed = 0.5f;

    private int firstTime = 0;
    private bool atBottom = false;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (errorMessagePanel == null || errorMessageText == null) Destroy(this.gameObject);

        //if (currentTime >= 0.0)
        //{
        //    currentTime -= Time.deltaTime;

        //    errorMessagePanel.color = setAlpha(errorMessagePanel.color, currentTime);
        //    errorMessageText.color = setAlpha(errorMessageText.color, currentTime);
        //}

        //if (!atBottom && firstTime > 0)
        //{
        //    Vector3 pos = errorMessagePanel.transform.position;
        //     pos.y = Vector3.MoveTowards(errorMessagePanel.gameObject.transform.position, infoPanel.transform.position, messageDropSpeed).y;
        //    errorMessagePanel.transform.position = pos;

        //    if ( (int)errorMessagePanel.transform.position.y+0.5f == (int)infoPanel.transform.position.y+0.5f )
        //    {
        //        atBottom = true;
        //        pos.y = infoPanel.transform.position.y;
        //        errorMessagePanel.transform.position = pos;
        //    }
        //}

	}

    public void showErrorMessage(HexNodeScript hexNode)
    {
        if (currentTime >= 0.0 && firstTime <= 1) return; 

        errorMessagePanel.gameObject.SetActive(true);
        int minWin = winCals.getMinSymbolsInWin(hexNode.tag);
        errorMessageText.text = "To remove " + hexNode.tag + "'s you need to connect at least " + minWin;

        errorMessagePanel.color = setAlpha(errorMessagePanel.color, 255);
        errorMessageText.color = setAlpha(errorMessageText.color, 255);

        currentTime = visibleTime;

        firstTime++;
    }

    private Color setAlpha(Color color, float value)
    {
        Color newColor = color;
        newColor.a = value;
        return newColor;
    }

    public void hideErrorPanel()
    {
        errorMessagePanel.gameObject.SetActive(false);
    }

}
