﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

/// <summary>
/// Stores the information about the player, who they are, how far they got and what level they have reached
/// </summary>
public class PlayerData : MonoBehaviour
{
    public static PlayerData playerData;

    public long challengeCount = 0;

    public enum GAME_TYPE
    {
        NONE,
        TITLE,
        STAGE,
        ENDLESS,
        CPU,
        HowTo
    };

    protected GAME_TYPE gameType = GAME_TYPE.NONE;
    public void setGameType(GAME_TYPE gt) { gameType = gt; }
    public GAME_TYPE getCurrentGameType() { return gameType; }

    //Current stage information
    public double currentScore = 0;
    public int currentMission = 0;

    //Current endless info
    public double endlessScore = 0;

    public double CPUScore = 0;
    
    /// <summary>
    /// Basic information about the game, this information wil be used to generate the game state.
    /// </summary>
    //PLAYER INFORMATION
    public int stageLevel;
    public double endlessHighScore;

    /// <summary>
    /// This is used for caluclations to determine challenges, if you aren't in stage mode then you use the current score to determine the value instead
    /// </summary>
    /// <returns></returns>
    public int getStageLevelCalculationComponent()
    {
        float timeSinceStart = challengeCount;
        int returnMe = isStageMode() ? (int)(stageLevel + (int)challengeCount) : (int)timeSinceStart;
        return returnMe;
    }


    public bool isStageMode()
    {
        return gameType == GAME_TYPE.STAGE;
    }

    public bool isTitleScreen()
    {
        return gameType == GAME_TYPE.TITLE;
    }

    public bool isEndlessMode()
    {
        return gameType == GAME_TYPE.ENDLESS;
    }

    public bool isCPUMode()
    {
        return gameType == GAME_TYPE.CPU;
    }

    public bool isHowToMode()
    {
        return gameType == GAME_TYPE.HowTo;
    }


    //------------------>>>
    //Challenge info//

    public static String ScoreText = "Score ";
    public static String ComboText = "Combo ";
    public static String RemoveText = "Remove ";
    public static String DestroyText = "Destroy all ";

    private string[] stageChallenges =
    {
        ScoreText,//If you get this, then you only have to get a certain amount of points
        ComboText,//You have to get a combo win of this size using this colour
        RemoveText,//You have to remove a certain amount of hexagons from the board.
        DestroyText//Remove all winning combinations of the colour from the screen
    };

    public string[] HexTypes =
    {
        "Yellow",
        "Green",
        "Orange",
        "Red",
        "Blue",
        "Purple",
    };
    
    private int minScoreToGet = 150;
    private int extraPerStageLevel = 8;

    private int maxComboChallenge = (CreateGrid.GRID_HEIGHT * CreateGrid.GRID_WIDTH) - 15;

    private string currentChallenge = "";

    //Extra info
    public string challengeColourType;
    public int challengeWinChain;
    public double challengeScoreTarget;
    public int challengeHexagonsDestroyed;//This is the total count of hexagons you kill, reset when a new challenge is generated.
    public int challengeTargetForDestroyHexagons;//Set this as the challenge target

    //When in a score challenge, this is the type that gets normal points, everything else is reduced by 1/4
    public string scoreFavouredHexType;
    public double scoreFavouredTypeIncrease = 1.60;
    public double scoreWeakTypeDecrease = 0.85;

    //-------------------------------------------------------------
    //WIN CALCULATIONS//

    //Each tag needs to have a minimum you need to win before you can count the amount you get as wins
    [NonSerialized]
    public int[] WinMinimums =
    {
        1,
        2,
        3,
        4,
        5,
        6,
    };

    //Each type is worth a base value, however many you get in the win is worth AMOUNT_IN_WIN * HexBaseWinValues
    [NonSerialized]
    public double[] HexBaseWinValues =
    {
        0.25,
        0.5,
        1.15,
        2,
        3.25,
        5
    };

    public void clearChallengeData()
    {
        challengeColourType = "";
        challengeWinChain = -1;
        challengeScoreTarget = -1;
        scoreFavouredHexType = "";
        currentChallenge = "";

        challengeHexagonsDestroyed = -1;
        challengeTargetForDestroyHexagons = -1;
    }

    void Awake()
    {
        //Creates a singleton of this class, you can only have 1 active player data class at once.
        if( playerData == null)
        {
            DontDestroyOnLoad(gameObject);
            playerData = this;

            getPlayerPrefs();

            clearChallengeData();
        }
        else if( playerData != this )
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Gets the information from the PlayerPrefs data storage
    /// </summary>
    public void getPlayerPrefs()
    {
        stageLevel = PlayerPrefs.GetInt("StageLevel");
        endlessHighScore = PlayerPrefs.GetInt("EndlessHighScore");
    }

    /// <summary>
    /// Stores the information for the game into the PlayerPrefs data storage
    /// </summary>
    public void storePlayerPrefs()
    {
        PlayerPrefs.SetInt("StageLevel", PlayerData.playerData.stageLevel);
        PlayerPrefs.SetFloat("EndlessHighScore", (float)PlayerData.playerData.endlessHighScore);

        PlayerPrefs.Save();
    }

    void Start()
    {

    }
    
    //********-------------------------------------------
    //CHALLENGE INFORMATION

    public string getCurrentChallengeText(bool getfake = false)
    {
        if (currentChallenge.StartsWith(RemoveText) && getfake ) return RemoveText + challengeHexagonsDestroyed + " more Hexagons";
        return currentChallenge;
    }

    public void loadNextChallenge(int seed)
    {
        challengeCount++;

        UnityEngine.Random.seed = seed;

        clearChallengeData();

        string finalString = "";
        string info = getChallengeInformation();

        if (info.StartsWith(ScoreText))
        {
            finalString = calculateScoreChallenge(info);
        }
        else if (info.StartsWith(ComboText))
        {
            finalString = calculateComboChallenge(info);
        }
        else if( info.StartsWith(RemoveText))
        {
            finalString = calculateRemoveChallenge(info);
        }
        else//Remove all of these colours from play
        {
            finalString = calculateDestroyChallenge(info);
        }

        currentChallenge = finalString;
    }

    /// <summary>
    /// Which challenge will you be trying to do?
    /// </summary>
    /// <returns></returns>
    private string getChallengeInformation()
    {
        int[] weightings = { 5, 40, 15, 1 };
        //Score, Combo, Remove, Destroy
        int[] stageChallengeIndexs = { 0, 1, 2, 3 };

        int index = WeightedRandom.ReturnRandomFromWeightings(weightings, stageChallengeIndexs);

        String info = stageChallenges[index];

        return info;
    }

    /// <summary>
    /// Calculate the score challenge based on your stage level
    /// </summary>
    /// <returns></returns>
    private string calculateScoreChallenge(string info)
    {
        double getThis = minScoreToGet;

        //For every stage level you should add an extra 150 
        double extra = extraPerStageLevel * getStageLevelCalculationComponent();

        getThis += extra;

        double gameModeCurrentScore = isStageMode() ? currentScore : endlessScore;
        getThis += gameModeCurrentScore;

        info = info + getThis.ToString();

        challengeScoreTarget = getThis;

        //Pick a colour to be strong, everything else is weak, worth a quarter the points
        string strongHexType = "";
        
        //Orange, Red, Blue, Purple
        int[] colourTypeIndexs = { 2, 3, 4, 5 };

        int[] weightings = { 1, 1, 1, 1};

        strongHexType = HexTypes[WeightedRandom.ReturnRandomFromWeightings(weightings, colourTypeIndexs)];

        scoreFavouredHexType = strongHexType;

        info += ". " + strongHexType + " is favoured";

        return info;
    }

    /// <summary>
    /// Calculates the colour that you need to combo, and the amount of them you need to get.
    /// The colour is affected by the stage level you are at, getting harder colours with longer chains as you level up
    /// The combo also increases per 5 levels.
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    private string calculateComboChallenge(string info)
    {
        //How much you need to combo, start at 1
        int amountInWin = (int)(getStageLevelCalculationComponent() * 1.15) + 2;
        
        int[] weightings = { 1, 0, 0, 0, 0, 0 };

        //Find out how much the min is for each and add 1 weighting for each
        for( int i =0; i < HexTypes.Length; i++)
        {
            int amount = WinCalculations.winCals.getMinSymbolsInWin(HexTypes[i]);
            if (amountInWin >= amount) weightings[i]+=(i+1* i)+1;
        }

        //Weightings to select which colour to select
        //Yellow, Green, Orange, Red, Blue, Purple
        int[] colourTypeIndexs = { 0, 1, 2, 3, 4, 5 };

        string colourType = HexTypes[WeightedRandom.ReturnRandomFromWeightings(weightings,colourTypeIndexs)];

        if (amountInWin > maxComboChallenge) amountInWin = maxComboChallenge;

        info = info + amountInWin.ToString() + " or more " + colourType.ToString() + "'s";

        challengeColourType = colourType;
        challengeWinChain = amountInWin;

        return info;
    }

    private int[] MinWinDivideByIncreaseAmount(int increaseAmount)
    {
        int[] clearance = new int[HexTypes.Length];
        for (int i = 0; i < clearance.Length; i++)
        {
            clearance[i] = increaseAmount / WinCalculations.winCals.getMinSymbolsInWin(HexTypes[i]);
        }
        return clearance;

    }

    private String calculateRemoveChallenge(string info)
    {
        challengeTargetForDestroyHexagons = UnityEngine.Random.Range(getStageLevelCalculationComponent(), (getStageLevelCalculationComponent() + 1) * 5);
        challengeHexagonsDestroyed = challengeTargetForDestroyHexagons;

        String text = RemoveText + challengeTargetForDestroyHexagons + " more Hexagons";

        return text;
    }


    private string calculateDestroyChallenge(string info)
    {
        //Every 10 levels you have to get 1 more extra in the combo
        int index = getStageLevelCalculationComponent() / 10;

        string colour = "";

        colour = HexTypes[UnityEngine.Random.Range(0,HexTypes.Length)];
        challengeColourType = colour;
        return info + colour;
    }



    ///////----------------------------------------------------------
    //GAME TIME
    
    /// <summary>
    /// Returns the amount of seconds you have
    /// </summary>
    /// <returns>time every round starts with</returns>
    public int getStartingRoundTime()
    {
        int stageMode = 12 + (int)(0.5 * (getStageLevelCalculationComponent() + 1));
        int cpuMode = 60;

        return isStageMode() ? stageMode : cpuMode;
    }

    //Each score is worth this many miliseconds to extend
    private float extendTimeScore = 0.5f;

    /// <summary>
    /// Calculates how much extra time you get based on the score you are adding in
    /// </summary>
    /// <param name="winPerHex"></param>
    /// <returns></returns>
    public float calculateTimeExtendFromScore(double winPerHex)
    {
        return (float)winPerHex * extendTimeScore;
    }







    ///////-----------------------------------------------------------
    ///STAGE MODE 
    /// 

    //Return true if completing this mission increased your stage level.
    public bool missionComplete()
    {
        if( challengeCount % 5 == 0 )
        {
            PlayerData.playerData.stageLevel++;
            return true;
        }
        return false;
    }
}
