﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class ShowStageLevel : MonoBehaviour
{

    //Stage level
    public Text stageLevelTextBox;

    // Use this for initialization
    void Start ()
    {
        stageLevelTextBox.text = "lvl: " + PlayerData.playerData.stageLevel.ToString();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void setCurrentStageLevel(int newStageLevel)
    {
        PlayerData.playerData.stageLevel = newStageLevel;
        stageLevelTextBox.text = PlayerData.playerData.stageLevel.ToString();
    }
}
