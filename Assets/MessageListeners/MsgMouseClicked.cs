﻿using UnityEngine;
using System.Collections;
using System;

public class MsgMouseClicked : MonoBehaviour
{
    public MoveHexagonHighlight highlightHexagon;

    public GameController gameController;

    public enum MOUSE_STATES
    {
        IDLE,
        HIGHLIGHT,
        CLICK,
        BOMB
    };

    public MOUSE_STATES mouseState = MOUSE_STATES.IDLE;

    private string bombColour = "";

    //MOUSE CLICKED TRACKING
    public bool mouseClicked = false;

    //MOUSE HOLD TRACKING
    public bool mouseDown = false;
    public bool mouseHeld = false;

    private float _startTime = 0.0f;

    public float _timeHeldDown = 0.0f;
    private float timeHeldDownThreshold = 0.5f;//Time in seconds while holding down to count as a highlight proc

    // Use this for initialization
    void Start ()
    {
	
	}

    /// <summary>
    /// Checks to see if the mouse button has been pushed down, is back up, or is being held
    /// </summary>
    private void detectMouseButtonStates()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseDown = true;
            mouseHeld = false;

            _timeHeldDown = 0.0f;

            if(mouseState != MOUSE_STATES.BOMB) mouseState = MOUSE_STATES.CLICK;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            mouseDown = false;
            mouseHeld = false;

            mouseClicked = true;

            checkMouseStateBehaviour();

            if (mouseState != MOUSE_STATES.CLICK) return;
        }
        
        if( mouseDown )
        {
            if (!mouseHeld && !mouseClicked )
            {
                _startTime = Time.time;
                _timeHeldDown += Time.deltaTime;
            }

            if (_timeHeldDown >= timeHeldDownThreshold)
            {
                mouseHeld = true;
                if (mouseState != MOUSE_STATES.BOMB) mouseState = MOUSE_STATES.HIGHLIGHT;
                checkMouseStateBehaviour();
            }
        }

    }

	
	// Update is called once per frame
	void Update ()
    {
        detectMouseButtonStates();
    }

    private void checkMouseStateBehaviour()
    {
        RaycastHit2D hit = objectAtMousePosition();
        if (hit.collider == null) return;

        HexNodeScript hexNode = hit.collider.gameObject.GetComponent<HexNodeScript>();
        if (hexNode == null) return;

        //You have clicked an active node
        if (hexNode.active)
        {
            //Always check to see if you are in the highlight stage
            if (mouseState == MOUSE_STATES.HIGHLIGHT)
            {
                checkIfNodeClickedIsWhereHighlightIs(hexNode);
                highlightHexagon.hexNodeClicked(hexNode);
            }
            else if (mouseClicked)
            {
                mouseClicked = false;
                if (mouseState == MOUSE_STATES.CLICK)
                {
                    if(highlightHexagon!=null)highlightHexagon.hexNodeWinClick(hexNode);
                    gameController.HexNodeClicked(hit.collider);
                }
                else if (mouseState == MOUSE_STATES.BOMB)
                {
                    mouseState = MOUSE_STATES.IDLE;
                    gameController.StoreBombClicked(this.bombColour);
                    gameController.HexNodeBombClicked(hit.collider);
                }
            }

        }
    }

    /// <summary>
    /// If the node you clicked already has a highlight on it then you are clicking to take that node, if its a different node then you are rehighlighting another
    /// </summary>
    /// <param name="hexNode"></param>
    private void checkIfNodeClickedIsWhereHighlightIs(HexNodeScript hexNode)
    {
        if (highlightHexagon == null) return;

        int[] gridpos = highlightHexagon.getHighlightGridPostiion();
        if (gridpos[0] != hexNode.gridX || gridpos[1] != hexNode.gridY)
        {
            mouseState = MOUSE_STATES.HIGHLIGHT;
        }
        else mouseState = MOUSE_STATES.IDLE;
    }

    public void setBombInformation(string bombColour)
    {
        this.bombColour = bombColour;
        mouseState = MOUSE_STATES.BOMB;
    }

    private RaycastHit2D objectAtMousePosition()
    {
        return Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
    }
}
