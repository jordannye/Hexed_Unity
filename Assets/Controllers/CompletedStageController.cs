﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CompletedStageController : MonoBehaviour
{
    private GameLoader _loader;

    public int showAdvertStageLevel = 5;

    public GameObject congratzScreen;

    public void challengeComplete(GameLoader loader)
    {
        _loader = loader;

        //if((PlayerData.playerData.stageLevel-1) % showAdvertStageLevel == 0 && PlayerData.playerData.stageLevel > 4)
        //{
        //    //Advertisement.Show();
        //}

        if (PlayerData.playerData.isStageMode())
        {
            if( PlayerData.playerData.missionComplete() )
            {
                congratzScreen.SetActive(true);
                CreateGrid.grid.destroyGridNodes();
                return;
            }
        }
        else if (PlayerData.playerData.isEndlessMode())
        {

        }
        loadNextChallenge();
    }

    public void returnToTitleScreen()
    {
        if (_loader != null )_loader.leaveStageMode(true);
    }

    public void loadNextChallenge()
    {
        if (_loader != null)
        {
            if (PlayerData.playerData.isStageMode())
            {
                _loader.leaveStageMode(true);
            }
            else if (PlayerData.playerData.isEndlessMode())
            {
                _loader.loadNextChallenge();
            }
        }
    }
}
