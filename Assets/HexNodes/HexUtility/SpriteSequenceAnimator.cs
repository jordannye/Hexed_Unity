﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpriteSequenceAnimator : MonoBehaviour
{
    public SpriteRenderer toAnimate;
    public Sprite[] sequence;


    public int frameRate;
    public int repetitions;
    public int timesRan;
    public bool reverse;

    private int startingFrame;

    public int progressCallbackFrameTrigger = 2;

    private int index;
    private float frameTime;
    private bool stop = true;

    private int putGridX, putGridY;

    private List<ICallback> progressCallbacks = new List<ICallback>();
    private List<ICallback> finishCallbacks = new List<ICallback>();
    private List<ICallback> startCallbacks = new List<ICallback>();

    private List<Action> finishActions = new List<Action>();

    public void setReversed(bool isReversed)
    {
        reverse = isReversed;

        if (reverse) index = sequence.Length - 1;

        if (progressCallbackFrameTrigger > sequence.Length - 1) progressCallbackFrameTrigger = sequence.Length - 1;
        if (progressCallbackFrameTrigger < 0) progressCallbackFrameTrigger = 0;

        startingFrame = index;
    }

    // Use this for initialization
    void Start ()
    {
        index = 0;
        if (reverse) index = sequence.Length - 1;

        if (progressCallbackFrameTrigger > sequence.Length - 1) progressCallbackFrameTrigger = sequence.Length - 1;
        if (progressCallbackFrameTrigger < 0) progressCallbackFrameTrigger = 0;

        startingFrame = index;

        timesRan = 0;
    }

    public void Begin()
    {
        for (int i = 0; i < startCallbacks.Count; i++)
        {
            startCallbacks[i].Run();
        }

        stop = false;
    }

    public void setGridPosition(int x, int y )
    {
        putGridX = x;
        putGridY = y;
    }

    /// <summary>
    /// Callbacks you can add to the animators
    /// </summary>
    /// <param name="callback"></param>

    public void addStartCallback(ICallback callback)
    {
        startCallbacks.Add(callback);
    }

    public void addFinishCallback(ICallback callback)
    {
        finishCallbacks.Add(callback);
    }

    public void addFinishAction(Action voidAction)
    {
        finishActions.Add(voidAction);
    }

    public void addProgressCallback(ICallback callback, int frameProgress = 3)
    {
        progressCallbackFrameTrigger = frameProgress;
        progressCallbacks.Add(callback);
    }

    public int getCurrentFrame()
    {
        return index;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (stop) return;

        frameTime += Time.deltaTime;

        if (frameTime >= (1.0f / frameRate) )
        {
            frameTime = 0.0f;

            //Choose which frame you want to choose, either forward or backwards
            if (!reverse)
            {
                index++;
                if (index >= sequence.Length)
                {
                    index = 0;
                    timesRan++;
                }

                if (index == progressCallbackFrameTrigger)
                {
                    triggerProgressCallback();
                }
            }
            else
            {
                index--;

                if (index == (sequence.Length - 1 - progressCallbackFrameTrigger))
                {
                    //startNextAnimator();
                    triggerProgressCallback();
                }

                if (index <= 0)
                {
                    index = sequence.Length-1;
                    timesRan++;
                }
            }

            //Check to see if you have ran the animation as many times as you wanted, if 0 ignore it and run forever.
            if (timesRan >= repetitions && repetitions != 0)
            {
                stop = true;
                animationFinished();
            }

            toAnimate.sprite = sequence[index];
        }

    }

    private void triggerProgressCallback()
    {
        for (int i = 0; i < progressCallbacks.Count; i++)
        {
            progressCallbacks[i].Run();
        }
    }

    private void animationFinished()
    {
        for (int i = 0; i < finishCallbacks.Count; i++)
        {
            finishCallbacks[i].Run();
        }

        for (int i = 0; i < finishActions.Count; i++)
        {
            finishActions[i].Invoke();
        }

        Destroy(gameObject);
    }

}
