﻿using UnityEngine;
using System.Collections;
using UnityEngine.Sprites;
using System;

public class ComputerPlayer : MonoBehaviour
{
    public GameController gameController;

    private int[] startingGridPosition = new int[] { CreateGrid.GRID_WIDTH / 2, CreateGrid.GRID_HEIGHT / 2 };//The Starting coords for where the CPU will be looking.

    [SerializeField]
    private float delayBetweenMovingSquares = 1.0f;//The delay before it can move to look at a neighting grid square

    public GameObject CPUHighlight;//This will be the sprite that will be put on the square the AI is currently looking at
    public Sprite clickedSprite;
    public Sprite hoverSprite;

    private float time = 0.0f;

    [SerializeField]
    private int[] _chosenGridPos;

    [SerializeField]
    private int[] _currentGridPos;



    //Create list of currently winning combinations
    private int[] currentWinCoords;// = new int[] { 0, 0 };

    public int[] winCoords;

	// Use this for initialization
	void Start ()
    {
        _currentGridPos = startingGridPosition;

        locateNewNodeToPress();
        putHighlightOnCurrentNode();

        //Starts the on going search for the best win, doesn't stop.
        StartCoroutine(generateListOfCurrentWins());
    }
	
	// Update is called once per frame
	void Update ()
    {
        time += Time.deltaTime;

        if( time >= delayBetweenMovingSquares )
        {
            time = 0.0f;
            moveTowardsTarget();
        }
	
	}

    /// <summary>
    /// Randomly picks a node on the grid to press.
    /// </summary>
    private void locateNewNodeToPress()
    {
        HexNodeScript node = null;
        if ( currentWinCoords != null )
        {
            GameObject obj = CreateGrid.grid.GAME_GRID[currentWinCoords[0], currentWinCoords[1]];
            if (obj != null)
            {
                node = obj.GetComponent<HexNodeScript>();
            }
        }

        if (node != null )
        {
            _chosenGridPos = new int[] { node.gridX,node.gridY };
        }
        else
        {
            _chosenGridPos = new int[] { (int)UnityEngine.Random.Range(0, CreateGrid.GRID_WIDTH), (int)UnityEngine.Random.Range(0, CreateGrid.GRID_HEIGHT) };
        }
    }

    /// <summary>
    /// Keeps randomly pick a place in the grid to check for a win, will store the place with the largest win then click there as its target.
    /// </summary>
    /// <returns></returns>
    IEnumerator generateListOfCurrentWins()
    {
        //Constantly looks for the best win on the grid, randomly selecting nodes and comparing it to the current win size you have.
        while (true)
        {
            int x = UnityEngine.Random.Range(0, CreateGrid.GRID_WIDTH);
            int y = UnityEngine.Random.Range(0, CreateGrid.GRID_HEIGHT);
            FindBiggestWin(x, y);
            yield return null;
        }
    }

    private void FindBiggestWin(int x, int y)
    {
        GameObject obj = CreateGrid.grid.GAME_GRID[x, y];
        ArrayList ry = GetAllNodesOfSameType.getNodes.getAllConnectingNodes(obj.tag, x, y);

        if (currentWinCoords == null) currentWinCoords = new int[] { x, y };

        if ( ry != null )
        {
            GameObject old = CreateGrid.grid.GAME_GRID[currentWinCoords[0], currentWinCoords[1]];
            ArrayList oldRy = GetAllNodesOfSameType.getNodes.getAllConnectingNodes(old.tag, x, y);
            
            if (oldRy == null || ry.Count > oldRy.Count)
            {
                currentWinCoords = new int[] { x, y };
                winCoords = currentWinCoords;
            }
        }
    }





    //-----------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Moves the highlight graphic over the current node the CPU is looking at
    /// </summary>
    private void putHighlightOnCurrentNode()
    {
        Vector3 vec = CreateGrid.grid.convertGridPositionToWorldPosition(_currentGridPos[0], _currentGridPos[1]);
        vec.z = -10;
        CPUHighlight.transform.position = vec;
    }

    /// <summary>
    /// This will move the highlight along the X first until it is at the correct X, then move along the Y.
    /// Once the highlight is at the end coordinate it will call @reachedSelectdChosenNode()
    /// </summary>
    private void moveTowardsTarget()
    {
        int moveX = checkToMoveX();
        _currentGridPos[0] = _currentGridPos[0] + moveX;

        if( moveX == 0 )
        {
            int moveY = checkToMoveY();
            _currentGridPos[1] = _currentGridPos[1] + moveY;

            if (moveY == 0)
            {
                reachedSelectedChosenNode();
            }
        }

        putHighlightOnCurrentNode();
    }

    private int checkToMoveX()
    {
        int moveX = 0;
        moveX = _chosenGridPos[0] < _currentGridPos[0] ? -1 : moveX;
        moveX = _chosenGridPos[0] > _currentGridPos[0] ? 1 : moveX;
        return moveX;
    }

    private int checkToMoveY()
    {
        int moveY = 0;
        moveY = _chosenGridPos[1] < _currentGridPos[1] ? -1 : moveY;
        moveY = _chosenGridPos[1] > _currentGridPos[1] ? 1 : moveY;
        return moveY;
    }

    /// <summary>
    /// Your highlighted node has reached the chosen node you want
    /// </summary>
    private void reachedSelectedChosenNode()
    {
        int x = _currentGridPos[0];
        int y = _currentGridPos[1];
        Collider2D col = CreateGrid.grid.GAME_GRID[x,y].gameObject.GetComponent<Collider2D>();

        gameController.HexNodeClicked(col,true);

        CPUHighlight.GetComponent<SpriteRenderer>().sprite = clickedSprite;

        StartCoroutine(ExecuteAfterTime(0.2f, clickAnimationComplete));
    }


    //-------------------------------------------------------------------------------------------

    private void clickAnimationComplete()
    {
        CPUHighlight.GetComponent<SpriteRenderer>().sprite = hoverSprite;

        delayBetweenMovingSquares -= 0.002f;
        if (delayBetweenMovingSquares < 0.1) delayBetweenMovingSquares = 0.1f;

        locateNewNodeToPress();
    }


    IEnumerator ExecuteAfterTime(float time, Action functionCall)
    {
        yield return new WaitForSeconds(time);

        functionCall.Invoke();
    }
}
